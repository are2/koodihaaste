﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Koodihaaste.BL;
using KoodihaasteML.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.ML;
using Newtonsoft.Json;

namespace Koodihaaste.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IBullshitService bullshitService;
        public bool UseMlNet => true;

        public IndexModel(IBullshitService bullshitService)
        {
            this.bullshitService = bullshitService;
        }

        public IEnumerable<Bullshit> BullshitsList => bullshitService.GetBullshitsList(UseMlNet);
        
    }
}
