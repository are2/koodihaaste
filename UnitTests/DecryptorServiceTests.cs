﻿using System;
using System.Collections.Generic;
using System.Linq;
using Koodihaaste.BL;
using NUnit.Framework;
using NUnit.Framework.Constraints;

namespace UnitTests
{
    [TestFixture]
    public class DecryptionServiceTests
    {
        private List<Tuple<string, string, int>> sampleTextsList;

        [SetUp]
        public void Setup()
        {
            sampleTextsList = new List<Tuple<string, string, int>>()
            {
                new Tuple<string, string, int>("Åcglsftfzzf vkvålåhhu ucrcffu zvzphhspzääååh qh rcrcf lzppuåcf åhp hpuhrpu åhyöpååhlzzh wpåff äzrhsåhh höhåh zääuzh rvrvärzpzzh.", "Työelämässä odotetaan nykyään sosiaalisuutta ja kykyä esiintyä tai ainakin tarvittaessa pitää uskaltaa avata suunsa kokouksissa.", 7),
                new Tuple<string, string, int>("", "", 7),
                new Tuple<string, string, int>("Dttxp dttvvztyl djvtdlwbltapvat xccbbcyppb xtpwpyzaztbcvapb dltvcbbtdlb wlclybltyl åjtdjwwj ölcsztbbcyppy.", "Viime viikkoina väkivaltaiseksi muuttuneet mielenosoitukset vaikuttivat lauantaina päivällä rauhoittuneen.", 11)
            };
        }

        [Test]
        public void TestCaesarDecryption()
        {
            //Arrange
            var decryptor = new CaesarDecryptionService();

            //Act
            var decryptedTexts = sampleTextsList.Select(textTuple => decryptor.Decrypt(textTuple.Item1, textTuple.Item3)).ToList();

            //Assert
            for (var i = 0; i < decryptedTexts.Count; i++)
            {
                Assert.AreEqual(sampleTextsList[i].Item2, decryptedTexts[i]);
            }
        }
    }
}