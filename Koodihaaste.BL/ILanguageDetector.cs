﻿namespace Koodihaaste.BL
{
    public interface ILanguageDetector
    {
        string Language { get; }
        double ValidateText(string text);
    }
}