﻿namespace Koodihaaste.BL
{
    public class LanguageDetectionResult
    {
        public LanguageDetectionResult(string detectedLanguage, double reliability)
        {
            this.DetectedLanguage = detectedLanguage;
            this.Reliability = reliability;
        }

        public string DetectedLanguage { get; }

        public double Reliability { get; }
    }
}