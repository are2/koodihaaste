﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Koodihaaste.BL
{
    public class FinnishDetector : ILanguageDetector
    {
        public string Language => "finnish";

        private List<string> suffixes = new List<string>()
        {
            "aali",
            "aani",
            "amiini",
            "an",
            "e",
            "edri",
            "eeni",
            "einen",
            "en",
            "fagi",
            "gate",
            "grammi",
            "han",
            "hko",
            "hkö",
            "hän",
            "idi",
            "in",
            "inen",
            "is",
            "isa",
            "isin",
            "ismi",
            "isti",
            "istinen",
            "isä",
            "itin",
            "ja",
            "jä",
            "ka",
            "kaan",
            "kas",
            "ke",
            "kin",
            "kka",
            "kko",
            "kku",
            "kky",
            "kkä",
            "kkö",
            "ko",
            "kratia",
            "ks",
            "ksi",
            "kä",
            "käs",
            "kään",
            "kö",
            "la",
            "lainen",
            "laisuus",
            "latria",
            "lepsia",
            "lla",
            "lle",
            "llinen",
            "llä",
            "logia",
            "lta",
            "lti",
            "ltä",
            "lyysi",
            "lä",
            "läinen",
            "läisyys",
            "mainen",
            "metri",
            "minen",
            "mme",
            "moinen",
            "mäinen",
            "möinen",
            "nen",
            "ni",
            "nne",
            "nsa",
            "nsä",
            "nyymi",
            "oa",
            "oidi",
            "oitin",
            "oittaa",
            "oli",
            "on",
            "oni",
            "onium",
            "os",
            "osainen",
            "pa",
            "pas",
            "pi",
            "pä",
            "päs",
            "ri",
            "si",
            "sidi",
            "silla",
            "sillaan",
            "sillään",
            "skooppi",
            "ssa",
            "sta",
            "sti",
            "sto",
            "stä",
            "stö",
            "ta",
            "tar",
            "ton",
            "tta",
            "ttaa",
            "tte",
            "ttä",
            "ttää",
            "tä",
            "tär",
            "ton",
            "tta",
            "ttaa",
            "tte",
            "ttä",
            "ttää",
            "tä",
            "tär",
            "tön",
            "un",
            "uri",
            "us",
            "uus",
            "yn",
            "yri",
            "ys",
            "yyni",
            "yys",
            "än",
            "öittää",
            "ön",
            "ös"
        };

        private double ValidateSuffixes(IEnumerable<string> words)
        {
            var wordsList = words.ToList();
            var numberOfWords = wordsList.Count();
            var numberOfMatchingSuffixes = wordsList.Count(word => suffixes.Any(suffix => word.EndsWith(suffix, true, CultureInfo.InvariantCulture)));

            return (double)numberOfMatchingSuffixes / numberOfWords;
        }

        private static double ValidateVowels(IEnumerable<string> words)
        {
            var numberOfInvalidVowels = 0;
            var wordsList = words.ToList();
            foreach (var word in wordsList)
            {
                var countA = word.Count(c => c == 'a') + word.Count(c => c == 'A');
                var countO = word.Count(c => c == 'o') + word.Count(c => c == 'o');
                var countU = word.Count(c => c == 'u') + word.Count(c => c == 'U');
                var countAa = word.Count(c => c == 'ä') + word.Count(c => c == 'Ä');
                var countOo = word.Count(c => c == 'ö') + word.Count(c => c == 'Ö');
                var countY = word.Count(c => c == 'y') + word.Count(c => c == 'y');
                var frontVowels = countA + countO + countU;
                var backVowels = countAa + countOo + countY;

                if (frontVowels > 0 && backVowels > 0 && !word.Equals("tällainen"))
                    numberOfInvalidVowels++;
            }

            return 1.0 - (double) numberOfInvalidVowels / wordsList.Count();
        }

        private static double ValidateBlacklisted(IEnumerable<string> words)
        {
            string[] blacklist = {
                "xx",
                "wc",
                "zz",
                "åw",
                "wx",
                "ww",
                "qq",
                "bq"
            };

            return words.Any(word => blacklist.Any(s => word.Contains(s, StringComparison.InvariantCultureIgnoreCase))) ? 0.0 : 1.0;
        }

        public double ValidateText(string text)
        {
            var words = text.Replace(".", "").Replace(",", "").Replace(":", "").Split(' ');
            var result = ValidateSuffixes(words) * ValidateVowels(words) * ValidateBlacklisted(words);
            return result;
        }
    }
}