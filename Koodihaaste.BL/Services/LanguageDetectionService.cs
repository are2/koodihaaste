﻿using System.Collections.Generic;

namespace Koodihaaste.BL
{
    public class LanguageDetectionService : ILanguageDetectionService
    {
        public IEnumerable<ILanguageDetector> LanguageDetectors { get; set; }

        public LanguageDetectionService()
        {
            LanguageDetectors = new List<ILanguageDetector>()
            {
                new FinnishDetector(),
                new BullshitDetector()
            };
        }

        public string GetLanguage(string text)
        {
            return GetLanguageDetectionResult(text).DetectedLanguage;
        }

        public LanguageDetectionResult GetLanguageDetectionResult(string text)
        {
            var detectedLanguage = "";
            var highest = 0.0;
            foreach (var detector in LanguageDetectors)
            {
                var res = detector.ValidateText(text);
                if (res > highest)
                {
                    highest = res;
                    detectedLanguage = detector.Language;
                }
            }

            return new LanguageDetectionResult(detectedLanguage, highest);
        }
    }
}
