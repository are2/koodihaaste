﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Koodihaaste.BL
{
    public class CaesarDecryptionService : IDecryptionService
    {
        public static string Alphabet => "abcdefghijklmnopqrstuvwxyzåäö";
        public string Decrypt(string cipherText, int key)
        {
            if (cipherText.Length < 1)
                return string.Empty;

            var decrypted = new List<char>();
            var currentCharNumber = 0;
            foreach (var c in cipherText)
            {
                var posInAlphabet = Alphabet.IndexOf(c, StringComparison.CurrentCultureIgnoreCase);
                if (posInAlphabet == -1)
                {
                    decrypted.Add(c);
                    continue;
                }

                var decryptedPos = posInAlphabet - key;
                if (decryptedPos < 0)
                    decryptedPos = Alphabet.Count() + decryptedPos;

                var decryptedChar = Alphabet[decryptedPos];
                if (char.IsLower(cipherText[currentCharNumber]))
                    decryptedChar = char.ToLower(decryptedChar);
                if (char.IsUpper(cipherText[currentCharNumber]))
                    decryptedChar = char.ToUpper(decryptedChar);

                decrypted.Add(decryptedChar);
                currentCharNumber++;
            }

            return string.Concat(decrypted);
        }
    }
}
