﻿using System.Collections.Generic;

namespace Koodihaaste.BL
{
    public interface ILanguageDetectionService
    {
        IEnumerable<ILanguageDetector> LanguageDetectors { get; set; }

        string GetLanguage(string text);

        LanguageDetectionResult GetLanguageDetectionResult(string text);
    }
}