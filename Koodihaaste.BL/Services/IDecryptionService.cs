﻿namespace Koodihaaste.BL
{
    public interface IDecryptionService
    {
        string Decrypt(string cipherText, int key);
    }
}