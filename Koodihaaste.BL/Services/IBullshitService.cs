﻿using System.Collections.Generic;

namespace Koodihaaste.BL
{
    public interface IBullshitService
    { 
        IEnumerable<Bullshit> GetBullshitsList(bool useMlNet);
    }
}