﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using KoodihaasteML.Model;
using Microsoft.ML;
using Newtonsoft.Json;

namespace Koodihaaste.BL
{
    public class BullshitService : IBullshitService
    {
        private readonly string accessToken;
        private readonly ILanguageDetectionService languageDetectionService;
        private readonly IDecryptionService decryptionService;

        public BullshitService(string accessToken, ILanguageDetectionService languageDetectionService, IDecryptionService decryptionService)
        {
            this.accessToken = accessToken;
            this.languageDetectionService = languageDetectionService;
            this.decryptionService = decryptionService;
        }


        public IEnumerable<Bullshit> GetBullshitsList(bool useMlNet)
        {
            // Create new MLContext
            var mlContext = new MLContext();

            // Load model & create prediction engine
            var modelPath = AppDomain.CurrentDomain.BaseDirectory + "MLModel.zip";
            var mlModel = mlContext.Model.Load(modelPath, out var modelInputSchema);
            var predEngine = mlContext.Model.CreatePredictionEngine<ModelInput, ModelOutput>(mlModel);

            var bss = GetBullshits().Result;
            if (bss == null)
                return Enumerable.Empty<Bullshit>();

            foreach (var bullshit in bss)
            {
                for (var i = 0; i < CaesarDecryptionService.Alphabet.Length; i++)
                {
                    var decrypted = decryptionService.Decrypt(bullshit.Message, i);
                    var input = new ModelInput
                    {
                        Language = "none",
                        Text = decrypted
                    };

                    if (useMlNet)
                    {
                        //Use trained ML.NET model for detection
                        var predictionResult = predEngine.Predict(input);
                        if (predictionResult.Prediction == "finnish")
                        {
                            bullshit.Message = decrypted;
                            bullshit.IsBullshit = false;
                            break;
                        }
                    }
                    else
                    {
                        //Use builtin language detector
                        if (languageDetectionService.GetLanguage(decrypted) == "finnish")
                        {
                            bullshit.Message = decrypted;
                            bullshit.IsBullshit = false;
                            break;
                        }
                    }
                }
            }

            return bss.Select(bs => bs);
        }


        public class BullshitsJson
        {
            public Bullshit[] Bullshits;
        }

        private async Task<Bullshit[]> GetBullshits()
        {
            const string uri = "https://koodihaaste-api.solidabis.com/bullshit";
            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + accessToken);
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var res = await response.Content.ReadAsStringAsync();
                var bullshits = JsonConvert.DeserializeObject<BullshitsJson>(res);
                return bullshits.Bullshits;
            }

            return null;
        }
    }
}